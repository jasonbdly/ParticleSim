'use strict';

import { dom } from '/js/domtool.js';

class Vector {
	constructor() {
		var args = Array.prototype.slice.call(arguments);
		if (Array.isArray(arguments[0])) {
			args = arguments[0];
		}
		this.vals = [];
		args.forEach((arg, i) => {
			this.vals[i] = arg;
		});
	}

	static Zero(size) {
		var vals = [];
		for (var i = 0; i < size; i++) {
			vals.push(0);
		}

		return new Vector(vals);
	}

	static Distance(vec1, vec2) {
		var resp = Vector.Zero(Math.max(vec1.vals.length, vec2.vals.length));
		return resp.AddV(vec2).SubV(vec1);
	}

	Set(i, val) {
		this.vals[i] = val;
		return this;
	}

	Get(i) {
		return this.vals[i];
	}

	X(val) {
		if (val) {
			return this.Set(0, val);
		}
		return this.Get(0);
	}

	Y(val) {
		if (val) {
			return this.Set(1, val);
		}
		return this.Get(1);
	}

	Z(val) {
		if (val) {
			return this.Set(2, val);
		}
		return this.Get(2);
	}

	W(val) {
		if (val) {
			return this.Set(3, val);
		}
		return this.Get(3);
	}

	AddV(otherVector) {
		this.vals = this.vals.map((val, i) => {
			return val + otherVector.Get(i);
		});
		return this;
	}

	AddS(scalar) {
		this.vals = this.vals.map((val, i) => {
			return val - scalar;
		});
		return this;
	}

	SubV(otherVector) {
		this.vals = this.vals.map((val, i) => {
			return val - otherVector.Get(i);
		});
		return this;
	}

	SubS(scalar) {
		this.vals = this.vals.map((val, i) => {
			return val - scalar;
		});
		return this;
	}

	MultS(scalar) {
		this.vals = this.vals.map(val => {
			return val * scalar;
		});
		return this;
	}

	DivS(scalar) {
		this.vals = this.vals.map(val => {
			return val / scalar;
		});
		return this;
	}

	Magnitude() {
		return Math.sqrt(Math.pow(this.X(), 2) + Math.pow(this.Y(), 2));
	}

	Unit() {
		var resp = this.Clone(),
			magnitude = resp.Magnitude();
		if (magnitude > 0) {
			resp.DivS(magnitude);
		}

		return resp;
	}

	Clone() {
		var resp = Vector.Zero(this.vals.length);
		resp.AddV(this);
		return resp;
	}
}

class Particle {
	constructor(props, arena) {
		this.position = Vector.Zero(2);
		this.velocity = Vector.Zero(2);
		for (var propName in props) {
			this[propName] = props[propName];
		}

		this.arena = arena;
	}

	Position() {
		return this.position;
	}

	X(val) {
		return this.position.X(val);
	}

	Y(val) {
		return this.position.Y(val);
	}

	Electric() {
		return this.electric;
	}

	Distance(otherParticle) {
		return Math.sqrt(Math.pow(otherParticle.X() - this.X(), 2) + Math.pow(otherParticle.Y() - this.Y(), 2));
	}

	Update(deltaTime) {
		var force = Vector.Zero(2);

		this.arena.GetParticles().forEach(otherParticle => {
			if (this != otherParticle) {
				var unit = Vector.Distance(this.Position(), otherParticle.Position()).Unit();
				var dist = this.Distance(otherParticle);
				dist = Math.max(dist, 1E-25);
				var eForce = ElectricForce(this.Electric(), otherParticle.Electric(), dist);

				var energyLost = otherParticle.energy * eForce * 1E-15;

				otherParticle.electric -= energyLost;
				this.electric += energyLost;

				if (eForce == Infinity || eForce == -Infinity || isNaN(eForce)) {
					console.log(dist, eForce);
					debugger;
				}

				var eForceVec = unit.MultS(eForce / this.mass);

				force.AddV(eForceVec);
			}
		});

		force.DivS(1E36);

		this.velocity.AddV(force);

		if (this.velocity.X() == Infinity || this.velocity.X() == -Infinity || isNaN(this.velocity.X()) ||
			this.velocity.Y() == Infinity || this.velocity.Y() == -Infinity || isNaN(this.velocity.Y())) {
			debugger;
		}

		this.position.AddV(this.velocity);
	}
}

class Electron extends Particle {
	constructor(arena) {
		super({
			electric: 1,
			mass: 9E-31
		}, arena);
	}
}

class Positron extends Particle {
	constructor(arena) {
		super({
			electric: -1,
			mass: 9E-30
		}, arena);
	}
}

class ElectricPit extends Particle {
	constructor(arena) {
		super({
			electric: -10,
			mass: Number.MAX_VALUE
		}, arena);
	}
}

class Arena {
	constructor(size, numParticles) {
		this.size = size;
		this.particles = [];
		for (var i = 0; i < numParticles / 2; i++) {
			var newParticle = new Electron(this);
			newParticle.X(Math.floor(Math.random() * this.size.X()));
			newParticle.Y(Math.floor(Math.random() * this.size.Y()));
			this.particles.push(newParticle);
		}
		for (var i = 0; i < numParticles / 2; i++) {
			var newParticle = new Positron(this);
			newParticle.X(Math.floor(Math.random() * this.size.X()));
			newParticle.Y(Math.floor(Math.random() * this.size.Y()));
			this.particles.push(newParticle);
		}
	}

	AddParticle(particle) {
		this.particles.push(particle);
	}

	Update(t) {
		this.particles.forEach(particle => {
			particle.Update(t || 1);

			if (particle.X() < 0 || particle.X() > this.size.X()) {
				particle.X(Math.abs(particle.X()) % this.size.X());
				//particle.X(Math.min(Math.max(particle.X(), 2), this.size.X() - 2));
				//particle.velocity.X(-particle.velocity.X());
			}

			if (particle.Y() < 0 || particle.Y() > this.size.Y()) {
				particle.Y(Math.abs(particle.Y()) % this.size.Y());
				//particle.Y(Math.min(Math.max(particle.Y(), 2), this.size.Y() - 2));
				//particle.velocity.Y(-particle.velocity.Y());
			}

			particle.velocity.MultS(0.95);

			//console.log("Particle Pos: ", particle.X(), particle.Y());
		});
	}

	Draw(gfx) {
		this.particles.forEach(particle => {
			var particleColor = CalcColor(particle);
			//gfx.fillStyle = 'rgba(' + particleColor.X() + ',' + particleColor.Y() + ',' + particleColor.Z() + ',1)';

			var size = CalcSize(particle);
			//gfx.fillRect(particle.X() - size.X() / 2, particle.Y() - size.Y() / 2, size.X(), size.Y());

			drawCircle(gfx, particle.X(), particle.Y(), size.X(), particleColor);
		});
	}

	GetParticles() {
		return this.particles;
	}
}

const CoulombConstant = 8.9875E9;
function ElectricForce(charge1, charge2, distance) {
	return CoulombConstant * charge1 * charge2 / Math.pow(distance, 2);
}

const GravityConstant = 6.67E-11;
function GravitationalForce(mass1, mass2, distance) {
	return GravityConstant * mass1 * mass2 / Math.pow(distance, 2);
}

function CalcColor(particle) {
	var red = Math.floor(Math.max(-particle.electric, 0) * 255);
	var green = Math.floor(0 * 255);
	var blue = Math.floor(Math.max(particle.electric, 0) * 255);

	return new Vector(red, green, blue);
}

const MaxSize = 40;
const MinSize = 6;
const SpeedCutoff = 1E10;
const MassCutoff = 1E10;
function CalcSize(particle) {
	var speedAffectorX = MaxSize * Math.min((Math.abs(particle.velocity.X()) / SpeedCutoff), 0.20);
	var speedAffectorY = MaxSize * Math.min((Math.abs(particle.velocity.Y()) / SpeedCutoff), 0.20);
	var massAffector =  (MaxSize * 0.8 * Math.min(particle.mass / MassCutoff, 1)) + MinSize;

	var width = Math.abs(massAffector - speedAffectorX);
	var height = Math.abs(massAffector - speedAffectorY);

	return new Vector(width, height);
}

function drawCircle(ctx, x, y, size, color) {
    ctx.beginPath();
    ctx.arc(x, y, size, 0, Math.PI*2);
    ctx.fillStyle = 'rgba(' + color.X() + ',' + color.Y() + ',' + color.Z() + ',1)';
    ctx.fill();
    ctx.closePath();
}

dom(document).ready(() => {
	var display = dom('#display');
	var draw = display.elem.getContext('2d');

	var displaySize = new Vector(1024, 720),
		simSize = new Vector(1024 * 2, 720 * 2);

	display.attr('width', simSize.X())
		.css('width', displaySize.X())
		.attr('height', simSize.Y())
		.css('height', displaySize.Y());

	var arena = new Arena(simSize, 150);

	var pit = new ElectricPit(arena);
	pit.X(2 * simSize.X() / 7).Y(simSize.Y() / 2);
	arena.AddParticle(pit);

	var pit = new ElectricPit(arena);
	pit.X(4 * simSize.X() / 7).Y(simSize.Y() / 2);
	arena.AddParticle(pit);

	var pit = new ElectricPit(arena);
	pit.X(6 * simSize.X() / 7).Y(simSize.Y() / 2);
	arena.AddParticle(pit);

	setInterval(() => {
		draw.clearRect(0, 0, simSize.X(), simSize.Y());
		arena.Update(1000 / 30);
		arena.Draw(draw);
	}, 1000 / 60);
});