package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"
)

func main() {
	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		log.Fatal(err)
	}

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		reqPath := r.URL.Path
		fullPath := dir + reqPath
		fileContents, err := ioutil.ReadFile(fullPath)
		contentType := http.DetectContentType(fileContents)
		if strings.HasSuffix(reqPath, ".js") {
			contentType = "application/javascript"
		}

		if err != nil {
			fmt.Println("File not found: ", fullPath)
		} else {
			fmt.Println("Got request to: ", fullPath)
		}
		fmt.Println("ContentType: ", contentType)
		w.Header().Set("Content-Type", contentType)
		w.Write(fileContents)
	})
	log.Fatal(http.ListenAndServe(":50142", nil))
}
